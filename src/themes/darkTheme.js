import { createMuiTheme } from "@material-ui/core";
import { teal, orange } from "@material-ui/core/colors";
const darkTheme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      light: teal[100],
      main: teal[500],
      dark: teal[900],
      contrastText: "#fff",
    },
    secondary: {
      light: orange[100],
      main: orange[500],
      dark: orange[900],
      contrastText: "#fff",
    },
    /* primary: {
      light: teal[100],
      main: teal[500],
      dark: teal[900],
      contrastText: "#fff",
    },
    secondary: {
      light: lime[100],
      main: lime[500],
      dark: lime[900],
      contrastText: "#fff",
    },
    success: {
      light: red[100],
      main: red[500],
      dark: red[900],
      contrastText: "#fff",
    },
    info: {
      light: red[100],
      main: red[500],
      dark: red[900],
      contrastText: "#fff",
    },
    warning: {
      light: red[100],
      main: red[500],
      dark: red[900],
      contrastText: "#fff",
    },
    error: {
      light: red[100],
      main: red[500],
      dark: red[900],
      contrastText: "#fff",
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)",
    },
    background: {
      paper: "#fff",
      default: "#fafafa",
    },*/
  },
});

export default darkTheme;
