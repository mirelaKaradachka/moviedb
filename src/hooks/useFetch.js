import { useState, useEffect, useCallback } from "react";
function useFetch(fetchCallback, deps = []) {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState();

  const initFetch = useCallback(fetchCallback, deps);

  useEffect(() => {
    setLoading(true);
    timeout(10000, initFetch())
      .then((response) => {
        setData(response);
        return response;
      })
      .catch((error) => {
        setError(error.message);
        console.error("Error in useFetch: ", error.message);
      })
      .finally(() => setLoading(false));
  }, [initFetch]);
  return [data, loading, error];
}
function timeout(ms, promise) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      reject(new Error("Request timeout"));
    }, ms);
    promise.then(resolve, reject);
  });
}

export { useFetch };
