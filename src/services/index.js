export { default as MovieService } from "./movie-service";
export { default as AuthService } from "./auth-servie";
export { default as LocalStorageService } from "./local-storage-service";
