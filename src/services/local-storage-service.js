const LocalStorageService = (function () {
  var _service;
  function _getService() {
    if (!_service) {
      _service = this;
      return _service;
    }
    return _service;
  }
  function _setToken(tokenObj) {
    localStorage.setItem("_access_token", tokenObj.access_token);
    localStorage.setItem("_refresh_token", tokenObj.refresh_token);
  }

  function _getAccessToken() {
    return localStorage.getItem("_access_token");
  }

  function _getRefreshToken() {
    return localStorage.getItem("_refresh_token");
  }
  function _clearToken() {
    localStorage.removeItem("_access_token");
    localStorage.removeItem("_refresh_token");
  }
  function _setUser(user) {
    localStorage.setItem("_user", JSON.stringify(user));
  }
  function _getUser() {
    return JSON.parse(localStorage.getItem("_user"));
  }
  function _clearUser() {
    localStorage.removeItem("_user");
  }
  return {
    getService: _getService,
    setToken: _setToken,
    getAccessToken: _getAccessToken,
    getRefreshToken: _getRefreshToken,
    clearToken: _clearToken,
    getUser: _getUser,
    setUser: _setUser,
    clearUser: _clearUser,
  };
})();

export default LocalStorageService;
