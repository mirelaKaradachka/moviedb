// export const BASE_URL = 'http://localhost:9000/api/comments';
// const BASE_API_URL = process.env.REACT_APP_API_URL;

// const API_URL = "http://www.omdbapi.com/?s=Batman&apikey=a318731d&i=tt1117563";
const dummyPoster =
  "https://www.allianceplast.com/wp-content/uploads/no-image.png";

// export const BASE_URL = process.env.REACT_APP_API_ENDPOINT;
export const BASE_URL = "http://localhost:4000";

class MoviesApi {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
  }

  /**
   * Get all movies
   */
  async getAllMovies() {
    const movieResp = await fetch(`${BASE_URL}/movies`);
    return await movieResp.json();
  }
  /**
   * Get movies by title search term.
   *
   * @param {*} searchTermTitle
   */
  async getMovieByTitle(searchTermTitle) {
    // let allData = [];
    const response = await fetch(
      `${BASE_URL}/movies?Title_like=${searchTermTitle}`
    );
    const data = await response.json();
    const formattedResult = data.map((element) => ({
      id: element.id,
      imdbID: element.imdbID,
      title: element.Title,
      year: element.Year,
      type: element.Type,
      poster: element.Poster === "N/A" ? dummyPoster : element.Poster,
    }));
    return formattedResult;
  }

  /**
   * Get movie by imdbID
   */
  async getMovieById(id) {
    const response = await fetch(`${BASE_URL}/movies/${id}?_embed=reviews`);
    const data = await response.json();
    const dataArr = Array.isArray(data) || [data];
    const formattedResult = dataArr.map((element) => ({
      id: element.id,
      imdbID: element.imdbID,
      title: element.Title,
      year: element.Year,
      type: element.Type,
      video: element.Video,
      poster: element.Poster === "N/A" ? dummyPoster : element.Poster,
      overview: element.Overview,
      reviews: element.reviews,
    }));
    return Array.isArray(formattedResult)
      ? formattedResult[0]
      : formattedResult;
  }

  async createReview(review) {
    const resp = await fetch(`${BASE_URL}/reviews`, {
      method: "POST",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(review),
    });
    const created = await resp.json();
    console.log("Review created", created);
    return created;
  }
  async deleteReview(review) {
    const resp = await fetch(`${BASE_URL}/reviews/${review.id}`, {
      method: "DELETE",
      mode: "cors",
    });
    const deleted = await resp.json();
    console.log("Review deleted", deleted);
    return deleted;
  }

  /**  http://localhost:4000/users/2/watchlist?_expand=movie
   *
   * @param {*} userID
   */
  async getUserWatchlist(userID) {
    const response = await fetch(
      `${BASE_URL}/users/${userID}/watchlist?_expand=movie`
    );
    //
    const data = await response.json();
    const formattedResult = data.map((element) => {
      const obj = element;
      const movie = element.movie
        ? {
            id: element.movie.id,
            imdbID: element.movie.imdbID,
            title: element.movie.Title,
            year: element.movie.Year,
            type: element.movie.Type,
            video: element.movie.Video,
            poster:
              element.movie.Poster === "N/A"
                ? dummyPoster
                : element.movie.Poster,
          }
        : null;
      return { ...obj, movie: movie };
    });
    return formattedResult;
  }
  async addUserWatchlistMovie(movieId, userId){
    const body = {movieId,userId};
  const resp = await fetch(`${BASE_URL}/watchlist`, {
     method: "POST",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(body),
    });
    const res = await resp.json();
    return res;
  }
  //   async createReview(review) {
  //   const resp = await fetch(`${BASE_URL}/reviews`, {
  //     method: "POST",
  //     mode: "cors",
  //     headers: { "Content-Type": "application/json" },
  //     body: JSON.stringify(review),
  //   });
  //   const created = await resp.json();
  //   console.log("Review created", created);
  //   return created;
  // }

  async removeUserWatchlistMovie(id){
  const resp = await fetch(`${BASE_URL}/watchlist/${id}`, {
      method: "DELETE",
      mode: "cors",
    });
    const deleted = await resp.json();
    return deleted;
  }
  async getUserProfile(userID) {
    const response = await fetch(`${BASE_URL}/users/${userID}`);
    //
    const data = await response.json();
    return data;
  }

  /**
   * Login and save jwt token localStorage
   */
  async login(email, password) {
    const credentials = { email: email, password: password };
    const resp = await fetch(`${BASE_URL}/login`, {
      method: "POST",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(credentials),
    });
    const jwt = await resp.json();
    const user = this.encodeToken(jwt);
    // encodedjwt {"email":"user@mail.com","iat":1603170084,"exp":1603173684,"sub":"2"}
    // localStorage.setItem("_accessToken", jwt ? jwt.accessToken : null);

    return user;
  }


  /**
   * Register and save jwt token localStorage
   */
  async register(
    email,
    password,
    firstname = "N/A",
    lastname = "N/A",
    role = "user"
  ) {
    const body = {
      email: email,
      password: password,
      firstname: firstname,
      lastname: lastname,
      role: role,
    };
    console.log("regbody", body);
    const resp = await fetch(`${BASE_URL}/register`, {
      method: "POST",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(body),
    });
    const jwt = await resp.json();
    const user = this.encodeToken(jwt);
    return user;
  }


  /**
  * Modify user profile
  */
  async modifyUser(user){
    const resp = await fetch(`${BASE_URL}/users/${user.id}`, {
      method: "PUT",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(user),
    });
     const result = await resp.json();
     console.log('Modified user',result);
     return result;
  }

  /**Get JWT accessToken and convert it to user with token, email, userId */
    encodeToken(jwt){
    const token = jwt.accessToken;
    console.log("service jwt", token);
    const encoded = JSON.parse(atob(token.split(".")[1]));
    console.log("encodedjwt", encoded);
    const user = {
      token: jwt.accessToken,
      email: encoded.email,
      userId: encoded.sub,
    }
    return user;
  }
}

export default new MoviesApi(BASE_URL);
//new MoviesApi(API_URL);
