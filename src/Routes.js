import React from "react";
import { BrowserRouter as Router, Switch,Redirect} from "react-router-dom";

import {
  MainLayout,
  RouteWithLayout,
  Login,
  Register,
  MovieSearch,
  MovieDetails,
  WatchList,
  UserProfile,
} from "./components";

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Redirect
        exact
        from="/"
        to="/home"
      />
        {/* <RouteWithLayout
          component={About}
          exact
          layout={MainLayout}
          path="/about"
          privateRoute={false}
        /> */}
        <RouteWithLayout
          component={MovieSearch}
          exact
          layout={MainLayout}
          path="/home"
          // privateRoute={false}
        />
        <RouteWithLayout
          component={Login}
          exact
          layout={MainLayout}
          path="/login"
          // privateRoute={false}
        />
        <RouteWithLayout
          component={Register}
          exact
          layout={MainLayout}
          path="/register"
          // privateRoute={false}
        />
        <RouteWithLayout
          component={UserProfile}
          exact
          layout={MainLayout}
          path="/profile"
          privateRoute={true}
        />

        <RouteWithLayout
          component={MovieDetails}
          // exact
          layout={MainLayout}
          path="/movie-details/:movieId"
          privateRoute={true}
        />
        <RouteWithLayout
          component={WatchList}
          exact
          layout={MainLayout}
          path="/watchlist"
          privateRoute={true}
        />
      </Switch>
    </Router>
  );
};

export default Routes;
