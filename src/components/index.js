export { default as RouteWithLayout } from "./RouteWithLayout";
export { default as MainLayout } from "./MainLayout";
export { default as MovieSearch } from "./MovieSearch";
export { default as MovieDetails } from "./MovieDetails";
export { default as WatchList } from "./WatchList";
export { default as Login } from "./Login";
export { default as Register } from "./Register";
export { default as UserProfile } from "./UserProfile";
export { default as Topbar } from "./Topbar";
