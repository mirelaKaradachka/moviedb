import React, { useContext, useState } from "react";
import {
  Button,
  Container,
  Grid,
  makeStyles,
  TextField,
  Typography,
  Link,
} from "@material-ui/core";
import { Link as RouterLink } from "react-router-dom";
import MOVIES_API from "../../services/movie-service";
import { UserContext } from "../../context/UserContext";
import LocalStorageService from "../../services/local-storage-service";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  container: {
    marginTop: 50,
    margin: "auto",
    textAlign: "center",
  },
  register: {
    marginTop: theme.spacing(2),
  },
}));

const Login = (props) => {
  const { history } = props;
  const classes = useStyles();
  const userContext = useContext(UserContext);
  const [state, setState] = useState({ email: "", password: "" });

  const handleInputChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.value });
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    // event.stopPropagation();
    await MOVIES_API.login(state.email, state.password)
      .then((res) => {
        console.log("login res", res);
        LocalStorageService.setUser(res);
        userContext.setUser(res);
        history.push("/");
        return res;
      })
      .catch((e) => console.error(e));
  };

  return (
    <Container className={classes.container} maxWidth="xs">
      <form onSubmit={handleSubmit} autoComplete="off">
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label="Email"
                  name="email"
                  size="small"
                  variant="outlined"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label="Password"
                  name="password"
                  size="small"
                  type="password"
                  variant="outlined"
                  autoComplete="new-password"
                  onChange={handleInputChange}
              />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Button
              color="secondary"
              fullWidth
              type="submit"
              variant="contained"
            >
              Log in
            </Button>
            <Typography
              color="textSecondary"
              className={classes.register}
              variant="body2"
            >
              Don't have an account?{" "}
              <Link
                component={RouterLink}
                to="/register"
                color="secondary"
                variant="subtitle1"
              >
                Register
              </Link>
            </Typography>
          </Grid>
        </Grid>
      </form>
    </Container>
  );
};

Login.propTypes = {};

export default Login;
