import React from "react";
import { makeStyles } from "@material-ui/core";
import Topbar from "../Topbar";

const useStyles = makeStyles(() => ({
  root: {
    height: "100%",
  },
  content: {
    height: "100%",
  },
}));

const MainLayout = (props) => {
  const { children } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Topbar />
      <main className={classes.content}>{children}</main>
    </div>
  );
};

MainLayout.propTypes = {};

export default MainLayout;
