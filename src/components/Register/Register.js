import React, { useState,useContext } from "react";
import {
  Button,
  Container,
  Grid,
  makeStyles,
  TextField,
  Typography,
  Link,
} from "@material-ui/core";
import { Link as RouterLink } from "react-router-dom";
import { UserContext } from "../../context/UserContext";
import MOVIES_API from "../../services/movie-service";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  container: {
    marginTop: 50,
    margin: "auto",
    textAlign: "center",
  },
  register: {
    marginTop: theme.spacing(2),
  },
}));
const Register = (props) => {
  const { history } = props;
  const classes = useStyles();
  const userContext = useContext(UserContext);
  const [state, setState] = useState({
    email: "",
    password: "",
    firstname: "",
    lastname: "",
    role: "user",
  });
  const handleInputChange = (event) => {
    console.log("Name", event.target.name, "   Value: ", event.target.value);
    setState({ ...state, [event.target.name]: event.target.value });
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    // event.stopPropagation();
    console.log("State", state);
    // localStorage.setItem("username", state.email || "test");
    // localStorage.setItem("_accessToken", "tmp");
     await MOVIES_API.register(
      state.email,
      state.password,
      state.firstname,
      state.lastname,
      state.role
    )
      .then((res) => {
        console.log(res);
        userContext.setUser(res);
        history.push("/");
        return res;
      })
      .catch((e) => console.error(e));
  };
  return (
    <Container className={classes.container} maxWidth="xs">
      <form onSubmit={handleSubmit}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label="Email"
                  name="email"
                  size="small"
                  variant="outlined"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label="Password"
                  name="password"
                  size="small"
                  type="password"
                  variant="outlined"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label="First Name"
                  name="firstname"
                  size="small"
                  variant="outlined"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label="Last Name"
                  name="lastname"
                  size="small"
                  variant="outlined"
                  onChange={handleInputChange}
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Button
              color="secondary"
              fullWidth
              type="submit"
              variant="contained"
            >
              Register
            </Button>
            <Typography
              color="textSecondary"
              className={classes.register}
              variant="body2"
            >
              Have an account?{" "}
              <Link
                component={RouterLink}
                to="/login"
                color="secondary"
                variant="subtitle1"
              >
                Log in
              </Link>
            </Typography>
          </Grid>
        </Grid>
      </form>
    </Container>
  );
};

Register.propTypes = {};

export default Register;
