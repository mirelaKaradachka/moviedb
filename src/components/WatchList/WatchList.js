import React,{ useContext,useState} from "react";
import MOVIES_API from "../../services/movie-service";
import { useFetch, } from "../../hooks/useFetch";
import { Container, Paper, List, ListItem,ListItemAvatar,ListItemText, makeStyles, Typography,Tooltip, IconButton,Divider } from "@material-ui/core";
import { Delete as DeleteIcon } from "@material-ui/icons";
import { UserContext } from "../../context/UserContext";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2)
  },
  status:{
    marginTop:theme.spacing(2),
    textAlign: 'center'
  },
  paper:{
    padding: theme.spacing(2),
    margin: "auto",
  },
  list: {
    flexGrow: 1,
  },
  item: {
    heigh: 60,
  },
  cols: {
    display: "flex",
    justifyContent: "space-between",
    marginLeft: theme.spacing(4)
  },
  poster: {
    heigh: 300,
    width: 150,
    borderRadius: 'none'
  },
}));

const WatchList = (props) => {
  const classes = useStyles();
  const userContext = useContext(UserContext);
  const [refetch, setRefetch] = useState(false);
  const [data, loading, error] = useFetch(() => MOVIES_API.getUserWatchlist(userContext.user.userId),  [refetch]);
  console.log("watchlist:", data);
  // const [movie, setMovie] = useState();

  const handleRemoveWatchlistMovie =  async (item) => {
    console.log('removed item',item);
    try{
    await MOVIES_API.removeUserWatchlistMovie(item.id);
     setRefetch(!refetch);
    }catch(e){
      console.log('Error removing favourite movie : ',e   );
    }
    // setRefetch(!refetch);
  };
  if (loading) {
    return <div className={classes.status}>Loading...</div>;
  }
  if (error) {
    return <div className={classes.status}>Error displaing data.</div>;
  }
  if (!data || data.length === 0) {
   return  <Container  maxWidth="md" className={classes.paper}>
        <Paper className={classes.paper}>
      <Typography variant="h4" style={{ cursor: "pointer" }}>
            WatchList
          </Typography>
         <div className={classes.status}>No data to display!</div>
         </Paper>
         </Container>;
  }
  return (
    <div className={classes.root}>
      <Container  maxWidth="md" className={classes.paper}>
        <Paper className={classes.paper}>
      <Typography variant="h4" style={{ cursor: "pointer" }}>
            WatchList
          </Typography>
      <List spacing={20} className={classes.list}>
        {data.map((item) => (
          <ListItem key={item.id} className={classes.item}>
            <ListItemAvatar>
              <img
                className={classes.poster}
                src={item.movie.poster}
                alt={item.movie.title}
              />
            </ListItemAvatar>
            <ListItemText
                primary={
                <div className={classes.cols}>
                   <Typography component="span" variant="h6" color="secondary">
                      { item.movie.title}
                    </Typography>
                     <Tooltip title="Remove from watchlist">
                      <IconButton
                        color="secondary"
                        aria-label="delete"
                        className={classes.iconButton}
                        onClick={(event) => {
                          handleRemoveWatchlistMovie(item);
                        }}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                  </div>
                }>
                </ListItemText>
              <Divider variant="inset"  />
          </ListItem>

        ))}
      </List>
      </Paper>
      </Container>
    </div>
  );
};

WatchList.propTypes = {};

export default WatchList;
