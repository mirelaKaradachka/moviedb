import React, { useContext, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import {
  AppBar,
  Button,
  Icon,
  IconButton,
  makeStyles,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { AccountCircle} from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import { UserContext } from "../../context/UserContext";
import LocalStorageService from "../../services/local-storage-service";
// import Icon from "@material-ui/icons/TheatersOutlined";

const useStyles = makeStyles((theme) => ({
  root: {
    // flexGrow: 1,
  },
  headerToolbar: {
    // width: "100%",
    display: "flex",
    justifyContent: "space-between",
  },
  headerLink: {
    textDecoration: "none",
    // display: "flex",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  headerButton: {
    color: "#fff",
  },
  title: {
    color: theme.palette.primary.contrastText,
    flexGrow: 1,
  },
  headerActions: {},
}));

const Topbar = (props) => {
  // const history = useHistory();
  const classes = useStyles();
  // const userContext = useContext(UserContext);
  // localStorage.getItem("_accessToken")
  // const [auth, setAuth] = useState(userContext.user.token !== null);



  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className={classes.headerToolbar}>
          {/* <Button
            className={classes.headerButton}
            to="/"
            component={RouterLink}
            startIcon={<Icon>theaters</Icon>}
          >
            Movie Database
          </Button> */}
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <Icon>theaters</Icon>
          </IconButton>
          <RouterLink to="/" color="primary" className={classes.headerLink}>
            <Typography variant="h6" className={classes.title}>
              Movie Database
            </Typography>
          </RouterLink>
<HeaderActions className="headerActions"/>
        </Toolbar>
      </AppBar>
    </div>
  );
};
const HeaderActions = (props) => {
   const history = useHistory();
  const classes = useStyles();
   const userContext = useContext(UserContext);
     const [hideActions] = useState(
    window.location.pathname.includes("/login") ||
      window.location.pathname.includes("/register")
  );
  // ["/login", "/register"].includes(window.location.pathname)
  console.log("hideActions", hideActions);
  console.log("user context", userContext.user);
  console.log("Show empty", hideActions && userContext.user !== {});
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOpenProfile = () => {
    setAnchorEl(null);
  };
  const handleLogout = () => {
    setAnchorEl(null);
    // localStorage.removeItem("username");
    // localStorage.removeItem("_accessToken");
    userContext.setUser(null);
    LocalStorageService.clearUser();
    history.push("/login");
  };
  // if(hideActions){
  //   return  <div></div>
  // }
   if(hideActions){
    return  <div></div>
  }
   if(userContext.user && userContext.user.email){
  return <div className="headerActions">
                <Button
                  className={classes.headerButton}
                  to="/watchlist"
                  component={RouterLink}
                  startIcon={<Icon>add_to_queue</Icon>}
                >
                  Watchlist
                </Button>
                <Button
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={handleMenu}
                  color="secondary"
                  endIcon={<AccountCircle />}
                >
                  {userContext.user.email}
                </Button>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={open}
                  onClose={handleClose}
                >
                  <MenuItem
                    to="/profile"
                    component={RouterLink}
                    onClick={handleOpenProfile}
                  >
                    Profile
                  </MenuItem>
                  <MenuItem
                    // to="/login"
                    // component={RouterLink}
                    onClick={handleLogout}
                  >
                    Logout
                  </MenuItem>
                </Menu>
          </div>
   }

  return (<Button
                className={classes.headerButton}
                to="/login"
                component={RouterLink}
                startIcon={<Icon>login</Icon>}
              >
                Login
              </Button>);};
Topbar.propTypes = {};

export default Topbar;
