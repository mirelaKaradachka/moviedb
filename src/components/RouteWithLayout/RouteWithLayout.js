import React, { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import { UserContext } from "../../context/UserContext";
// import LocalStorageService from "../../services/local-storage-service";
// import { LocalStorageService } from "services";

const RouteWithLayout = (props) => {
  const {
    layout: Layout,
    component: Component,
    privateRoute = false,
    ...rest
  } = props;
  const userContext = useContext(UserContext);

  const redirectToLogin = privateRoute && !userContext.user.token;

  if (redirectToLogin) {
    return <Redirect to="/login" />;
  }

  return (
    <Route
      {...rest}
      render={(matchProps) => (
        <>
          <Layout />
          <div>
            <Component {...matchProps} />
          </div>
        </>
      )}
    />
  );
};

RouteWithLayout.propTypes = {
  component: PropTypes.any.isRequired,
  layout: PropTypes.any.isRequired,
  path: PropTypes.string,
  privateRoute: PropTypes.bool,
};

// RouteWithLayout.defaultProps = {
//   privateRoute: false,
// };

export default RouteWithLayout;
