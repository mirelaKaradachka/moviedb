import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { MovieList, SearchInput } from "./componets";
import MOVIES_API from "../../services/movie-service";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  content: {
    margin: "auto",
    maxWidth: "80vw",
    textAlign: "center",
  },
}));

const MovieSearch = (props) => {
  const classes = useStyles();
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setHasError] = useState(false);

  /**
   * Handle change of search input
   * @param {*} event
   */
  const handleChange = (event) => {
    setSearchTerm(event.target.value);
  };

  /**
   * Handle search form submit.
   * Call movie API with the value from search field.
   * Update search result with the response.
   * @param {*} event
   */
  const handleSearch = (event) => {
    setIsLoading(true);
    MOVIES_API.getMovieByTitle(searchTerm)
      // fetchData(searchTerm)
      .then((response) => {
        const results = response || [];
        setIsLoading(false);
        //Sort response by title in ascending order
        response.sort((a, b) => (a.title > b.title ? 1 : -1));
        setSearchResults(results);
        return results;
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
        setIsLoading(false);
        setHasError(true);
      });

    event.preventDefault();
    event.stopPropagation();
  };

  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <SearchInput
          value={searchTerm}
          handleChange={handleChange}
          handleSubmit={handleSearch}
        />
        <MovieList
          data={searchResults}
          isLoading={isLoading}
          error={hasError}
        />
      </div>
    </div>
  );
};

export default MovieSearch;
