import React from 'react';
import { AppBar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(2)
    },
}));

const Header = (props) => {
    const classes = useStyles();
    return (
        <AppBar position="sticky" className={classes.root}>
            <Typography variant="h6">
                Movie search engine
          </Typography>
        </AppBar>
    );
};

export default Header;