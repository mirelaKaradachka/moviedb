import React from 'react';
import { Paper, IconButton, InputBase } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: theme.spacing(2),
        padding: theme.spacing(1),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputField: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
}));

const SearchInput = props => {
    const { value, handleChange, handleSubmit } = props;
    const classes = useStyles();

    return (
        <Paper component="form" className={classes.root} onSubmit={handleSubmit}>
            <InputBase
                className={classes.inputField}
                placeholder="Search movie"
                inputProps={{ 'aria-label': 'search-movie' }}
                value={value}
                onChange={handleChange}
            />
            <IconButton type="submit" className={classes.iconButton} aria-label="search">
                <SearchIcon color="primary" />
            </IconButton>
        </Paper>
    );
};


export default SearchInput;