import React, { useContext, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import {
  GridList,
  GridListTile,
  GridListTileBar,
  Icon,
  IconButton,
  Tooltip,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  MoreOutlined as MoreOutlinedIcon,
} from "@material-ui/icons";
import { UserContext } from "../../../../context/UserContext";
// import InfoIcon from "@material-ui/icons/Info";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(1),
  },
  status: {
    marginTop: theme.spacing(2),
  },
  tile: {
    "&:hover": {
      backgroundColor: "red",
    },
  },
  poster: {
    maxWidth: "100%",
    height: "auto",
  },
}));

const MovieList = (props) => {
  const { data, isLoading, error } = props;
  const classes = useStyles();
  const userContext = useContext(UserContext);
  const [auth] = useState(userContext.user && userContext.user.hasOwnProperty("token"));
  // const handleViewDetails = () => {
  //   browserHistory.push("/movie-details/test");
  // };

  if (isLoading) {
    return <div className={classes.status}>Loading...</div>;
  }
  if (error) {
    return <div className={classes.status}>Error displaing data.</div>;
  }
  if (!data || !data.length) {
    return <div className={classes.status}>No data to display!</div>;
  }

  return (
    <GridList cols={4} cellHeight={180} spacing={20} className={classes.root}>
      {data.map((item) => (
        <GridListTile key={item.id} className={classes.tile}>
          {/* <div> */}
          <img className={classes.poster} src={item.poster} alt={item.title} />
          {/* </div> */}
          <GridListTileBar
            title={item.title}
            subtitle={<span>year: {item.year}</span>}
            actionIcon={
              auth ? (
                <Tooltip title="View movie details">
                  <IconButton
                    to={`/movie-details/${item.id}`}
                    component={RouterLink}
                    aria-label={`info about ${item.title}`}
                    className={classes.icon}
                  >
                    {/* <Icon>more</Icon> */}
                    <MoreOutlinedIcon />
                  </IconButton>
                </Tooltip>
              ) : (
                <Tooltip title="Login to view movie details">
                  <IconButton
                    // color="secondary"
                    aria-label={`info about ${item.title}`}
                    className={classes.icon}
                  >
                    {/* <BlockIcon /> */}
                    <Icon>lock</Icon>
                  </IconButton>
                </Tooltip>
              )
            }
          />
        </GridListTile>
      ))}
    </GridList>
  );
};

export default MovieList;
