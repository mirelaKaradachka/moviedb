export { default as Header } from './Header';
export { default as SearchInput } from './SearchInput';
export { default as MovieList } from './MovieList';