import React, { useState, useContext, useEffect } from "react";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Container,
  Divider,
  Grid,
  makeStyles,
  TextField,
} from "@material-ui/core";
import MOVIES_API from "../../services/movie-service";
import { useFetch } from "../../hooks/useFetch";
import { UserContext } from "../../context/UserContext";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4),
  },
}));

const UserProfile = (props) => {
  const classes = useStyles();
  //get user from context
  const userContext = useContext(UserContext);
  console.log("userId", userContext.user);
  const [data, loading, error] = useFetch(() =>
    MOVIES_API.getUserProfile(userContext.user.userId)
  );
  const [values, setValues] = useState({});
  useEffect(() => {
    setValues(data);
  }, [data]);
  console.log("User data,", data);
  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const roles = [
    {
      value: "user",
      label: "User",
    },
    {
      value: "admin",
      label: "Admin",
    },
    {
      value: "reviewer",
      label: "Reviewer",
    },
  ];

  const handleSubmit = async(event) => {
    event.preventDefault();
    event.stopPropagation();
    await MOVIES_API.modifyUser(values);
    console.log("handleSubmit", values);
  };
  if (loading) {
    return <div className={classes.status}>Loading...</div>;
  }
  if (error) {
    return <div className={classes.status}>Error displaing data.</div>;
  }
  if (!data) {
    return <div className={classes.status}>No data to display!</div>;
  }
  return (
    <Container maxWidth="md">
      <Card className={classes.root}>
        <form autoComplete="off" noValidate onSubmit={handleSubmit}>
          <CardHeader
            subheader="The information can be edited"
            title="Profile"
          />
          <Divider />
          <CardContent>
            <Grid container spacing={3}>
              <Grid item md={6} xs={12}>
                <TextField
                  fullWidth
                  helperText="Please specify the first name"
                  label="First name"
                  margin="dense"
                  name="firstname"
                  onChange={handleChange}
                  required
                  value={values.firstname}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <TextField
                  fullWidth
                  label="Last name"
                  margin="dense"
                  name="lastname"
                  onChange={handleChange}
                  required
                  value={values.lastname}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <TextField
                  fullWidth
                  label="Email Address"
                  margin="dense"
                  name="email"
                  onChange={handleChange}
                  required
                  value={values.email}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <TextField
                  fullWidth
                  label="Select role"
                  margin="dense"
                  name="role"
                  onChange={handleChange}
                  required
                  select
                  // eslint-disable-next-line react/jsx-sort-props
                  SelectProps={{ native: true }}
                  value={values.role}
                  variant="outlined"
                >
                  {roles.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </TextField>
              </Grid>
              {/* <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Country"
                margin="dense"
                name="country"
                onChange={handleChange}
                required
                value={values.country}
                variant="outlined"
              />
            </Grid> */}
            </Grid>
          </CardContent>
          <Divider />
          <CardActions>
            <Button color="secondary" variant="contained" type="submit">
              Save details
            </Button>
          </CardActions>
        </form>
      </Card>
    </Container>
  );
  // };
};

UserProfile.propTypes = {};

export default UserProfile;
