import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import MOVIES_API from "../../services/movie-service";
import { useFetch } from "../../hooks/useFetch";

const MovieSearch = (props) => {
  const [movies, setMovies] = useState([
    { imdbID: 1, title: "1a" },
    { imdbID: 2, title: "2a" },
  ]);
  // useEffect(() => {
  //   async function fetchData() {
  //     const def = [
  //       { imdbID: 1, title: "def1" },
  //       { imdbID: 2, title: "def2" },
  //     ];
  //     const res = (await MOVIES_API.getAllMovies()) | [];
  //   }
  //   fetchData();
  // }, []);

  const fetchData = async () => {
    const response = await MOVIES_API.getAllMovies();
    setMovies(response.data);
  };
  useEffect(() => {
    fetchData();
  }, []);

  // const [data = [], loading, error] = useFetch(() => MOVIES_API.getAllMovies());

  // if (loading) {
  //   return <div>Loading</div>;
  // }
  // if (error) {
  //   return <div>Error</div>;
  // }
  return (
    <div>
      <h4>Tutorials List</h4>

      <ul className="list-group">
        {movies ? (
          movies.map((movie) => <li key={movie.imdbID}>{movie.title}</li>)
        ) : (
          <div>No data</div>
        )}
        {/* {movies &&
          movies.map((movie, index) => <li key={index}>{movie.title}</li>)} */}
      </ul>
    </div>
  );
};

MovieSearch.propTypes = {};

export default MovieSearch;
