import React, { useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link as RouterLink } from "react-router-dom";
import {
  Container,
  Paper,
  Grid,
  ButtonBase,
  Typography,
  Tooltip,
  Fab,
  IconButton
} from "@material-ui/core";
import {
  ArrowBack as ArrowBackIcon,
  Favorite as FavoriteIcon,
} from "@material-ui/icons";
import MOVIES_API from "../../services/movie-service";
import { useFetch } from "../../hooks/useFetch";
import MovieReview from "./MovieReview";
import { UserContext } from "../../context/UserContext";
// import { MovieReview } from "./";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: "auto",
    // maxWidth: 500,
  },
  image: {
    width: 300,
    height: 400,
  },
  img: {
    margin: "auto",
    display: "block",
    maxWidth: "100%",
    maxHeight: "100%",
  },
  fab: {
    marginRight: theme.spacing(2),
  },
  overview: {
    marginTop: 50,
  },
}));
const MovieDetails = (props) => {
  const { match } = props;
  const classes = useStyles();
  const userContext = useContext(UserContext);
  const movieId = match.params.movieId || "";
  const [refetch, setRefetch] = useState(false);
  // const user = userContext.user;
  const handleCreateReview = async (review) => {

    //TODO get current user from context and change createdBy and userId
    const enchantedReview = {
      ...review,
      createdOn: new Intl.DateTimeFormat("en-US").format(new Date()),
      // createdBy: `${userContext.user.firstname} ${userContext.user.lastname}`,
      createdBy: userContext.user.email,
      userId: userContext.user.userId || 2,
      movieId: parseInt(movieId)
    };
    await MOVIES_API.createReview(enchantedReview);
    setRefetch(!refetch);
  };
  const handleDeleteReview = async (review) => {
    await MOVIES_API.deleteReview(review);
    setRefetch(!refetch);
  };
const handleAddToWatchlist = async (movieId, userId) => {
    await MOVIES_API.addUserWatchlistMovie(movieId, userId);
  };
  const [data, loading, error] = useFetch(
    () => MOVIES_API.getMovieById(movieId),
    [refetch]
  );

  if (loading) {
    return <div className={classes.status}>Loading...</div>;
  }
  if (error) {
    return <div className={classes.status}>Error displaing data.</div>;
  }
  if (!data) {
    return <div className={classes.status}>No data to display!</div>;
  }
  return (
    <div className={classes.root}>
      <Container maxWidth="md" className={classes.paper}>
        <Paper className={classes.paper}>
          <IconButton to={`/`} component={RouterLink}>
            <ArrowBackIcon />
          </IconButton>
          <Grid container spacing={2}>
            <Grid item>
              <ButtonBase className={classes.image}>
                <img className={classes.img} alt="Poster" src={data.poster} />
              </ButtonBase>
            </Grid>
            <Grid item xs={12} sm container>
              <Grid item xs container direction="column" spacing={2}>
                <Grid item xs>
                  <Typography variant="h5" gutterBottom>
                    {data ? data.title : "N/A"}
                  </Typography>
                  <Typography variant="subtitle1">
                    imdbID: {data.imdbID}
                  </Typography>

                  <Typography
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                  >
                    Year: {data.year}
                  </Typography>
                  <Typography variant="h6" className={classes.overview}>
                    Overview:
                  </Typography>
                  <Typography variant="subtitle1">{data.overview}</Typography>
                </Grid>
                {/* <Grid>
                  <Card>
                    <CardMedia
                      component="iframe"
                      height="140"
                      src={data.video}
                      title={data.title}
                    />
                  </Card>
                </Grid> */}
              </Grid>
              <Grid item>
                <Tooltip title="Add to watchlist" aria-label="add">
                  <Fab color="secondary" className={classes.fab} onClick={(event) => {
                          handleAddToWatchlist(data.id, userContext.user.userId);
                        }}>
                    {/* <Icon>add_to_queue</Icon> */}
                    <FavoriteIcon />
                  </Fab>
                </Tooltip>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </Container>
      <Container maxWidth="md" className={classes.paper}>
        <Paper className={classes.paper}>
          <Typography variant="h4" style={{ cursor: "pointer" }}>
            Reviews
          </Typography>
          <MovieReview
            reviews={data.reviews}
            onCreateReview={handleCreateReview}
            onDeleteReview={handleDeleteReview}
          />
        </Paper>
      </Container>
    </div>
  );
};

MovieDetails.propTypes = {};

export default MovieDetails;
