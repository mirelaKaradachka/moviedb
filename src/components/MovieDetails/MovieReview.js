import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Rating from "@material-ui/lab/Rating";
import {
  Avatar,
  Box,
  Button,
  Chip,
  Divider,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Paper,
  TextField,
  Tooltip,
  Typography
} from "@material-ui/core";
import { Star as StarIcon, Delete as DeleteIcon } from "@material-ui/icons";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

const labels = {
  0.5: "Useless",
  1: "Useless+",
  1.5: "Poor",
  2: "Poor+",
  2.5: "Ok",
  3: "Ok+",
  3.5: "Good",
  4: "Good+",
  4.5: "Excellent",
  5: "Excellent+",
};
const useStyles = makeStyles((theme) => ({
  root: {
    alignItems: "center",
    padding: theme.spacing(2),
  },
  reviewList: {
    width: "100%",
  },
  cols: {
    display: "flex",
    justifyContent: "space-between",
  },
  rows: {},
  form: {
    margin: theme.spacing(2),
  },
  formHeader: {
    display: "flex",
    justifyContent: "space-between",
  },
  ratingBox: {
    textAlign: "center",
  },
  iconButton: {
    height: 48,
    width: 48,
    alignSelf: "center",
  },
}));

const MovieReview = (props) => {
  const { reviews, onCreateReview, onDeleteReview } = props;
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <ReviewInput onCreateReview={onCreateReview} />
      <ReviewList reviews={reviews} onDeleteReview={onDeleteReview} />
    </div>
  );
};

const ReviewInput = ({ onCreateReview }) => {
  const [state, setState] = React.useState({
    rating: 5,
    createdOn: "",
    createdBy: "",
    reviewTitle: "",
    reviewComment: "",
  });
  // const [value, setValue] = React.useState(2);
  const [hover, setHover] = React.useState(-1);
  const classes = useStyles();
  const handleInputChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.value });
  };
  const handleSubmitReview = (event) => {
    event.preventDefault();
    event.stopPropagation();
    onCreateReview(state);
  };
  return (
    <div className={classes.root}>
      <form
        className={classes.form}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmitReview}
      >
        <Grid container direction="column" spacing={2}>
          <Grid item>
            <div className={classes.formHeader}>
              <h2>Write a review</h2>
              <Tooltip title="Rate it" placement="top-end">
                <div className={classes.ratingBox}>
                  <Rating
                    id="rating"
                    value={state.rating}
                    precision={0.5}
                    name="rating"
                    size="large"
                    onChange={handleInputChange}
                    onChangeActive={(event, newHover) => {
                      setHover(newHover);
                    }}
                  />
                  {state.rating !== null && (
                    <Box ml={2}>
                      {labels[hover !== -1 ? hover : state.rating]}
                    </Box>
                  )}
                </div>
              </Tooltip>
            </div>
          </Grid>
          <Grid item>
            <TextField
              id="review-title"
              fullWidth
              name="reviewTitle"
              label="Review title"
              variant="outlined"
              onChange={handleInputChange}
            />
          </Grid>
          <Grid item>
            <TextField
              id="review-comment"
              fullWidth
              name="reviewComment"
              label="Review"
              multiline
              rows={4}
              variant="outlined"
              onChange={handleInputChange}
            />
          </Grid>
          <Grid item>
            <Button
              color="secondary"
              fullWidth
              type="submit"
              variant="contained"
            >
              Submit review
            </Button>
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

const ReviewList = (props) => {
  const { reviews, onDeleteReview } = props;
  const classes = useStyles();
  if (!reviews) {
    return <></>;
  }
  const handleDeleteReview = (review) => {
    onDeleteReview(review);
  };
  return (
    <Paper square>
      <List className={classes.reviewList}>
        {reviews.map((review) => (
          <div key={review.id}>
            <ListItem>
              <ListItemAvatar>
                <Avatar color="primary">
                  <AccountCircleIcon color="inherit" />
                </Avatar>
              </ListItemAvatar>

              <ListItemText
                primary={
                  <div className={classes.cols}>
                    <Typography component="span" variant="h6" color="secondary">
                      {review.reviewTitle}
                    </Typography>
                    <Chip
                      size="small"
                      variant="outlined"
                      color="secondary"
                      icon={<StarIcon />}
                      label={review.rating}
                    />
                  </div>
                }
                secondary={
                  <div className={classes.cols}>
                    <div className={classes.rows}>
                      <Typography variant="subtitle2">
                        Written by {review.createdBy}
                        {" on "}
                        {new Intl.DateTimeFormat("en-US").format(
                          new Date(review.createdOn)
                        )}
                      </Typography>
                      <Typography
                        component="span"
                        variant="body2"
                        color="textPrimary"
                      >
                        {review.reviewComment}
                      </Typography>
                    </div>
                    <Tooltip title="Delete">
                      <IconButton
                        color="secondary"
                        aria-label="delete"
                        className={classes.iconButton}
                        onClick={(event) => {
                          handleDeleteReview(review);
                        }}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                  </div>
                }
              />
            </ListItem>
            <Divider variant="inset" component="li" />
          </div>
        ))}
      </List>
    </Paper>
  );
};

MovieReview.propTypes = {};

export default MovieReview;
