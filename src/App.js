import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Routes from "./Routes";
import { ThemeProvider } from "@material-ui/core";
import { theme } from "./themes";
import { UserProvider } from "./context/UserContext";

function App() {
  return (
    <>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <UserProvider>
          <Routes />
        </UserProvider>
      </ThemeProvider>
    </>
  );
}

export default App;
